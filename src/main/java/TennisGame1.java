
public class TennisGame1 implements TennisGame {

    private int scorePlayer1 = 0;
    private int scorePlayer2 = 0;
    private String player1Name;
    private String player2Name;

    public TennisGame1(String player1Name, String player2Name) {
        this.player1Name = player1Name;
        this.player2Name = player2Name;
    }

    public void wonPoint(String playerName) {
        if (player1Name.equals(playerName)) {
            scorePlayer1 += 1;
        } else {
            scorePlayer2 += 1;
        }
    }


    public String empate() {

        switch (scorePlayer1) {
            case 0:
                return "Love-All";
            case 1:
                return "Fifteen-All";

            case 2:
                return "Thirty-All";

            default:
                return "Deuce";
        }

    }

    public String ventaja() {

        int minusResult = scorePlayer1 - scorePlayer2;
        switch (minusResult) {
            case 1:
                return "Advantage player1";
            case 2, 3, 4:
                return "Win for player1";
            case -1:
                return "Advantage player2";
            default:
                return "Win for player2";

        }

    }

    public String generalScore() {
        String score = "";
        int temporal_Score = 0;
        for (int i = 1; i < 3; i++) {
            if (i == 1) temporal_Score = scorePlayer1;
            else {
                score += "-";
                temporal_Score = scorePlayer2;
            }
            switch (temporal_Score) {
                case 0:
                    score += "Love";
                    break;
                case 1:
                    score += "Fifteen";
                    break;
                case 2:
                    score += "Thirty";
                    break;
                case 3:
                    score += "Forty";
                    break;
            }
        }
        return score;
    }

    public String getScore() {

        if (scorePlayer1 == scorePlayer2) {
            return empate();
        }
        if (scorePlayer1 >= 4 || scorePlayer2 >= 4) {
            return ventaja();
        }
        return generalScore();
    }

}
